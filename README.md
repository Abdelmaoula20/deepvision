"# DeepVisionECL" 


links to Xu et al code [link](https://gitlab.inria.fr/yixu/deepmot)

Liens vers des sources utiles pour le projet :

[A friendly introduction to Siamese Networks](https://towardsdatascience.com/a-friendly-introduction-to-siamese-networks-85ab17522942)

[Recurrent Neural Networks](https://towardsdatascience.com/recurrent-neural-networks-d4642c9bc7ce)

Dataset qui sera utilisé : MOT17 de MOTChallenge